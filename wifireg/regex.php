<!DOCTYPE html>
<html lang="en" ng-app="wifiregapp" ng-controller="wifiregcontroller">
	<head>
		<title>Internet Registration Form</title>
		<link rel="shortcut icon" href="assets/img/icon.png" />
	 	
		<!--Fetching CSS and JS files-->
		<?php 
			include 'deploymod.php';
			echo '<script src="'.$url.'assets/js/jquery-2.1.3.min.js"></script>';
			echo '<script src="'.$url.'assets/js/angular.min.js"></script>';
			echo '<link rel="stylesheet" href="'.$url.'assets/css/bootstrap.min.css">';
		?>
		
		
	</head>
	<body>
		<!--Start of wifireg app-->
				<form class="wifireg_form" name="wifi_form" ng-submit="validate_user()">
					<input type="text" class="username" placeholder="Seraph Id" maxlength="20" required ng-model="username" autofocus>
					<span ng-if="username_empty">Please fill the username field</span>
					<span ng-if="password_empty">Please fill the username field</span>
					<div><button class="wel_screen_button validate_user bring_up" >&#62 </button></div>
				</form>
		<script>
			var app = angular.module('wifiregapp',[])
			.controller('wifiregcontroller',function($scope,$location,$window){
				
				// $window.location.href = 'intern.php';
				
				$scope.validate_user = function(){
					// var pat = /[a-zA-Z0-9]*[@][a-zA-Z0-9]*(.com|.org)$/;
					var pat = /^[A-Za-z]+(\s[A-Za-z]+)*$/;
					var res = pat.test($scope.username);
					alert(res);
				};
				
				$scope.reset = function(){
					$scope.username = "";
					$scope.password = "";
				};
				
				$scope.$watch(function(scope){
					return $scope.username;
					},function(val){
					if(val != '' && val != undefined){
						$scope.user = val + "'s Internet Registration Form";
					}
					else{
						$scope.user = "Internet Registration Form";
					}
				});
			});
		</script>
		<!--End of wifireg app-->
	</body>
</html>															