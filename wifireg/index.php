<!DOCTYPE html>
<html lang="en" ng-app="wifiregapp" ng-controller="wifiregcontroller">
	<head>
		<title>Internet Registration Form</title>
		<link rel="shortcut icon" href="assets/img/icon.png" />
	 	
		<!--Fetching CSS and JS files-->
		<?php 
			include 'deploymod.php';
			echo '<script src="'.$url.'assets/js/jquery-2.1.3.min.js"></script>';
			echo '<script src="'.$url.'assets/js/angular.min.js"></script>';
			echo '<script src="'.$url.'assets/js/jquery.backstretch.min.js"></script>';
			echo '<link rel="stylesheet" href="'.$url.'assets/css/bootstrap.min.css">';
			echo '<link rel="stylesheet" href="'.$url.'assets/css/chang.css">';
		?>
		
		<script type="text/javascript">
			$(document).ready(
			function()
			{
				$.backstretch([
				"<?php echo 'assets/img/paris.jpg'; ?>",
				"<?php echo 'assets/img/sea.jpg'; ?>"
				],{duration : 3000,fade : 1000});
				
				// setInterval(function(){
					// if ($(".validate_user").css("background-color") == "#7cd0f8") {
						// $(".validate_user").css("background-color","#F07818");
					// } else {
						// $(".validate_user").css("background-color","#7cd0f8");
					// }
				// },4000);
				
				$(".login_form_box").css("height",$(window).height());
				$(".password").css("opacity","0");
				$(".password").css("left","0px");
				$(".password").css("top","-80px");
				$(".username").focus();
				
				$(".username").click(function(){
					$(this).focus();
				});
				$(".password").click(function(){
					$(this).focus();
				});
				
				$(".reset").click(function(){
					$(".password").css("opacity","0");
					$(".password").css("left","0px");
					$(".password").css("top","-80px");
					$(".username").css("top","0px");
					$(".username").css("left","0px");
					$(".username").css("opacity","1");
					$(".username").focus();
				});
				
				$(".wifireg_form").submit(function(data){
					if($(".username").css("opacity") != "0" && $(".username").val() != ""){
						var regex = /^[a-zA-Z0-9]*$/;
						var res = regex.test($(".username").val());
						if (res) {
							$(".username").css("left","0px");
							$(".username").css("top","35px");
							$(".username").css("opacity","0");
							$(".password").css("opacity","1");
							$(".password").css("top","-45px");
							$(".password").focus();
							} else {
							$(".error_notif").addClass('error_notif_show');
							$(".error_notif h6").html("Invalid Register number");
							setTimeout(function(){
								$(".error_notif").removeClass('error_notif_show');
							},3000);
						}
					}
					else if($(".password").val() != ""){
						$(".password").removeClass("shake");
						$("body").removeClass("red");
						$("button").removeClass("shake_button");
						$(".password").focus();
						$.ajax({
							url: 'check.php',
							type: 'post',
							data: {
								username: $(".username").val(),
								password: $(".password").val()
							},
							success: function(val){
								if(val.search("success") != -1)
								{
									$.ajax({
										url: 'url.php',
										type: 'post',
										success: function(val){
											window.location.href = val;
										}
									});
								}
								else if(val.search("error") != -1){
									$(".error_notif").addClass('error_notif_show');
									$(".error_notif h6").html("Invalid Credentials!");
									setTimeout(function(){
										$(".error_notif").removeClass('error_notif_show');
									},3000);
									$(".password").addClass("shake");
									$("button").addClass("shake_button");
									$("body").addClass("red");
								}
								
							}
						});
					}
				});				
			});
		</script>
		
	</head>
	<body>
		<!--Start of wifireg app-->
		<div class="error_notif centered">
			<h6></h6>
		</div>
		
		<section class="login_form_box centered centered_col col-sm-12">
			<img src="assets/img/seraph.png" class="logo">
			<br>
			<div class="gray_box centered centered_col">
				<span class="wifireg_text centered user_bind">
					{{user}}
				</span>
				<form class="wifireg_form">
					<input type="text" class="username" placeholder="Seraph Id" maxlength="10" ng-model="username" >
					<input type="password" class="password" placeholder="Password" ng-model="password">
					<span ng-if="username_empty">Please fill the username field</span>
					<span ng-if="password_empty">Please fill the username field</span>
					<div>
						<button class="wel_screen_button validate_user bring_up" ng-click="validate_user()">&#62 </button>
					</div>
				</form>
			</div>
			<div class="wifireg_text centered reset">
				<a href="" ng-click="reset()">Sign In as a different user</a>
			</div>
			<div class="footer">
				<footer class="centered">
					2015 Computer Technology Centre
				</footer>
			</div>
		</section>
		
		<script>
			var app = angular.module('wifiregapp',[])
			.controller('wifiregcontroller',function($scope,$location,$window){
				
				// $window.location.href = 'intern.php';
				
				$scope.reset = function(){
					$scope.username = "";
					$scope.password = "";
				};
				
				$scope.$watch(function(scope){
					return $scope.username;
					},function(val){
					if(val != '' && val != undefined){
						$scope.user = val + "'s Internet Registration Form";
					}
					else{
						$scope.user = "Internet Registration Form";
					}
				});
			});
		</script>
		<!--End of wifireg app-->
	</body>
</html>															