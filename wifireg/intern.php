<!DOCTYPE html>
<html lang="en" ng-app="wifiregapp" ng-controller="wifiregcontroller" ng-init="load_json()">
	<head>
		<title>Student Internet Registration Form</title>
		<link rel="shortcut icon" href="assets/img/icon.png" />
	 	
		<!--Fetching CSS and JS files-->
		<?php 	
			include 'deploymod.php';
			echo '<script src="'.$url.'assets/js/jquery-2.1.3.min.js"></script>';
			echo '<script src="'.$url.'assets/js/angular.min.js"></script>';
			echo '<script src="'.$url.'assets/js/angular-route.min.js"></script>';
			echo '<script src="'.$url.'assets/js/bootstrap.min.js"></script>';
			echo '<script src="'.$url.'assets/js/intern.js"></script>';
			echo '<link rel="stylesheet" href="'.$url.'assets/css/bootstrap.min.css">';
			echo '<link rel="stylesheet" href="'.$url.'assets/css/netreg.css">';
		?>
		
		<script type="text/javascript">
			$(document).ready(
			function()
			{
				$(".fill_page").css("height",$(window).height());
				// $(".personal_details,.devices").click(function(){
				// $(".page_load_pre_props").addClass("page_load_post_props");
				// });
			}
			);
		</script>
		<!--script type="text/javascript">
			angular.element(document.getElementsByTagName('head')).append(angular.element('<base href="' + window.location.pathname + '" />'));
		</script-->
	</head>
	<body>
		<!--Start of wifireg app-->
		
		<div class="error_notif centered" ng-class="error_notif_show">
			<h6>{{notif_message}}</h6>
		</div>
		
		<section class="col-sm-12 nomargin fill_page">
		<nav class="navbar">
				<div class="container-fluid">
					<ul class="nav navbar-nav main_navbar">
						<li class="active centered centered_col">
							<img src="assets/img/loogo.png" class="header_logo">
						</li>
					</ul>
				</ul>
			</div>
		</nav>
		<div ng-view class="">
		</div>
		
		<div class="footer">
			<footer class="centered">
				2015 Computer Technology Centre
			</footer>
		</div>
	</section>
	<!--End of wifireg app-->
</body>
</html>																					