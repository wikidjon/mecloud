<!DOCTYPE html>
<html lang="en" ng-app="wifiregapp" ng-controller="wifiregcontroller">
	<head>
		<title>Student Internet Registration Form</title>
		<link rel="shortcut icon" href="assets/img/icon.png" />
	 	
		<!--Fetching CSS and JS files-->
		<?php 
			$dom = $_SERVER['SERVER_NAME'];
			echo '<script src="http://'.$dom.'/wifireg/assets/js/jquery-2.1.3.min.js"></script>';
			echo '<script src="http://'.$dom.'/wifireg/assets/js/angular.min.js"></script>';
			echo '<script src="http://'.$dom.'/wifireg/assets/js/angular-route.min.js"></script>';
			echo '<script src="http://'.$dom.'/wifireg/assets/js/bootstrap.min.js"></script>';
			echo '<script src="http://'.$dom.'/wifireg/assets/js/script.js"></script>';
			echo '<link rel="stylesheet" href="http://'.$dom.'/wifireg/assets/css/bootstrap.min.css">';
			echo '<link rel="stylesheet" href="http://'.$dom.'/wifireg/assets/css/netreg.css">';
		?>
		
		<script type="text/javascript">
			$(document).ready(
			function()
			{
				// $(".personal_details,.devices").click(function(){
					// $(".page_load_pre_props").addClass("page_load_post_props");
				// });
			}
			);
		</script>
		<script type="text/javascript">
			angular.element(document.getElementsByTagName('head')).append(angular.element('<base href="' + window.location.pathname + '" />'));
		</script>
	</head>
	<body>
		<!--Start of wifireg app-->
		<section class="col-sm-12 nomargin">
			<nav class="navbar">
				<div class="container-fluid">
					<ul class="nav navbar-nav main_navbar">
						<li class="active">
							<span><b>Internet Registration Form</b></span>
						</li>
					</ul>
					
					
					<ul class="nav navbar-nav navbar-right main_navbar">
						<li class="active">
							<a href="personal_details" class="personal_details">
								Personal Details <span class="sr-only">(current)</span>
							</a>
						</li>
						<li>
							<a href="devices" class="devices">
								Devices
							</a>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Jonathan <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#">Settings</a></li>
								<li><a href="#">Logout</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
			
			<div ng-view class="page_padding">
			</div>
			
			<div class="footer">
				<footer class="centered">
					2015 Computer Technology Centre
				</footer>
			</div>
		</section>
		<!--End of wifireg app-->
	</body>
</html>																					