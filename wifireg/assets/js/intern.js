var app = angular.module('wifiregapp',['ngRoute'])
.constant('userDataArray',{	
	"data0": "$rootScope.user_data_devices.data[index].name",
	"data3": "Wireless_MAC"
})
.controller('wifiregcontroller',function($scope,Session,$window,$http,$location,$timeout,$rootScope,$interval,$route,userDataArray) {
	//Variable declarations
	
	//Route change watching
	$rootScope.$on('$routeChangeStart',function(event, next, current){
		//Unauthorized users
		if(next.templateUrl === 'app/gotcha.html' && !$rootScope.user_details_filled){
			event.preventDefault();
		}
		else if(next.templateUrl === 'app/devices.html' && !$rootScope.agreed_itpolicy){
			event.preventDefault();
		}
		else if(next.templateUrl === 'app/usage.html' && !$rootScope.user_details_filled){
			event.preventDefault();
		}
		else if(next.templateUrl === 'app/gotcha.html' && $rootScope.user_details_filled && $rootScope.agreed_itpolicy){
			event.preventDefault();
		}
		
	});
	
	$scope.evaluate_data = function() {
		//Email icon
		$rootScope.home_icon_help = "assets/img/email.png";
		
		$rootScope.user_details_filled = true;
		angular.forEach($rootScope.user_data.data,function(value,key) {
			if (value.value == "") {
				$rootScope.user_details_filled = false;
				$rootScope.home_icon_user = "assets/img/user_warning.png";
				$rootScope.home_icon_devices = "assets/img/laptop_trans.png";
				$rootScope.home_icon_usage = "assets/img/usage_trans.png";
				$rootScope.index_notification_you = "Woah! Looks like you've got some blank fields. Fill up your details to enable the devices tab.";
			}
			$i = $i + 1;
		});
		
		if ($rootScope.user_details_filled) {
			$rootScope.home_icon_user = "assets/img/user_ok.png";
			$rootScope.home_icon_devices = "assets/img/laptop.png";
			$rootScope.home_icon_usage = "assets/img/usage.png";
			$rootScope.index_notification_you = "Your details look good! Click below to view them.";
		}
	};
	
	$scope.evaluate_data_devices = function() {
		$rootScope.user_details_devices_filled = [];
		angular.forEach($rootScope.user_data_devices.data,function(value,key){
			var index = key;
			angular.forEach(value,function(value,key){
				if(value == ""){
					$rootScope.user_details_devices_filled[index] = false;	
				}
			});
		});
	};
	
	//Auto route
	$scope.load_json = function() {
		
		if ($location.host() == "localhost") {
			// $rootScope.url = '';
			$rootScope.url = '';
			} else {
			// $rootScope.url = '';
			$rootScope.url = '';
		}
		
		$i = 0;
		$url = $rootScope.url + 'load_json.php';
		$http({
			url: $url,
			method: 'GET',
			params: {
				val: "hello"
			}
			}).success(function(val) {
			if (val.status == "success"){
				$rootScope.user_data = val;
				$scope.evaluate_data(val);
				} else {
				$scope.set_notif("Nope! Couldn't get data from server! Please try again later.''");
				$scope.dismiss_notif(3000);
			}
		});
		$location.path('/index');
	};
	
	$scope.load_json_devices = function() {
		
		$http({
			url:  $rootScope.url + 'load_json_devices.php',
			method: 'GET',
			params:{
				val:"hello"
			}
			}).success(function(val){
			if (val.status == "success") {
				$rootScope.user_data_devices = val;
				$scope.evaluate_data_devices();
				} else {
				$scope.set_notif("Nope! Couldn't get data from server! Please try again later.");
				$scope.dismiss_notif(3000);
			}
		});
	};
	
	$scope.dismiss_notif = function(delay) {
		$timeout(
		function() {
			$rootScope.error_notif_show = "";
			$rootScope.notif_message = "";
		},delay
		);
	};
	
	$scope.set_notif = function(msg) {
		$rootScope.error_notif_show = "error_notif_show";
		$rootScope.notif_message = msg;
	};
	
	$scope.dismiss_notif_modal = function(delay) {
		$timeout(
		function() {
			$rootScope.error_notif_show = "";
			$rootScope.notif_message_modal = "";
		},delay
		);
	};
	
	$scope.set_notif_modal = function(msg) {
		$rootScope.error_notif_show = "error_notif_show";
		$rootScope.notif_message_modal = msg;
	};
	
	$scope.view_device_details = function(type,data,index) {
		$rootScope.currently_editing_device = data;
		$rootScope.currently_editing_device_type = type;
		$rootScope.currently_editing_device_index = index;
	};
	
	$scope.set_for_edit_devices = function(data,index) {
		$scope.currently_editing = data;
		$scope.currently_editing_index = index;
	};
	
	$scope.update_data_devices = function(index,type) {
		var regex_name = /^[a-zA-Z^0-9_]+(\s[a-zA-Z^0-9]+)*$/;
		var regex_mac_unit = /^[a-fA-F^0-9]{2}$/;
		var regex_mobile= /^[0-9]{10}$/;
		if (type == 'phone' && confirm("Press OK if you're sure of the details provided")) {
			// angular.forEach([$scope.updated[0],$scope.updated[1],$scope.updated[2],$scope.updated[4],$scope.updated[5],$scope.updated[8],$scope.updated[9]],function(value,key){
			// if (value != undefined) {
			// $rootScope.user_data_devices.data[data].userDataArray.data3 = value;
			// }
			// });
			if ($scope.updated_name != undefined) {
				var mac_cool = true;
				angular.forEach($scope.updated_name,function(value,key){
					if (!regex_mac_unit.test(value)) {
						mac_cool = false;
					}
				});
				if (mac_cool) {
					$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].name = $scope.updated_name;
					} else {
					if($rootScope.notif_message_modal == undefined || $rootScope.notif_message_modal == '') {
						$scope.set_notif_modal("Invalid Name");
						} else {
						var msg = $rootScope.notif_message_modal + ', Name';
						$scope.set_notif_modal(msg);
					}
					$scope.dismiss_notif_modal(3000);
				}
			}
			if ($scope.updated_serial != undefined)
			
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Serial = $scope.updated_serial;
			if ($scope.updated_wireless_mac != undefined){
				var mac_cool = true;
				angular.forEach($scope.updated_wireless_mac,function(value,key){
					if (!regex_mac_unit.test(value)) {
						mac_cool = false;
					}
				});
				if (mac_cool) {
					$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Wireless_MAC = $scope.updated_wireless_mac;
					} else {
					if($rootScope.notif_message_modal == undefined || $rootScope.notif_message_modal == '') {
						$scope.set_notif_modal("Invalid Wireless Mac Id");
						} else {
						var msg = $rootScope.notif_message_modal + ', Wireless Mac Id';
						$scope.set_notif_modal(msg);
					}
					$scope.dismiss_notif_modal(3000);
				}
			}
			
			if ($scope.updated_manufacturer != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Manufacturer = $scope.updated_manufacturer;
			if ($scope.updated_model != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Model = $scope.updated_model;
			if ($scope.updated_imei != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].IMEI = $scope.updated_imei;
			if ($scope.updated_mobile_number != undefined){
				var mac_cool = true;
				if (!regex_mobile.test($scope.updated_mobile_number)) {
					mac_cool = false;
				}
				if (mac_cool) {
					$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Mobile_Number = $scope.updated_mobile_number;
					} else {
					if($rootScope.notif_message_modal == undefined || $rootScope.notif_message_modal == '') {
						$scope.set_notif_modal("Invalid Mobile Number");
						} else {
						var msg = $rootScope.notif_message_modal + ', Mobile Number';
						$scope.set_notif_modal(msg);
					}
					$scope.dismiss_notif_modal(3000);
				}
			}
			} else if (type == 'tablet' && confirm("Press OK if you're sure of the details provided")) {
			if ($scope.updated_name != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].name = $scope.updated_name;
			if ($scope.updated_serial != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Serial = $scope.updated_serial;
			if ($scope.updated_wireless_mac != undefined){
				var mac_cool = true;
				angular.forEach($scope.updated_wireless_mac,function(value,key){
					if (!regex_mac_unit.test(value)) {
						mac_cool = false;
					}
				});
				if (mac_cool) {
					$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Wireless_MAC = $scope.updated_wireless_mac;
					} else {
					if($rootScope.notif_message_modal == undefined || $rootScope.notif_message_modal == '') {
						$scope.set_notif_modal("Invalid Wireless MAC Id");
						} else {
						var msg = $rootScope.notif_message_modal + ', Wireless MAC Id';
						$scope.set_notif_modal(msg);
					}
					$scope.dismiss_notif_modal(3000);
				}
			}
			if ($scope.updated_manufacturer != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Manufacturer = $scope.updated_manufacturer;
			if ($scope.updated_model != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Model = $scope.updated_model;
			
			} else if (type == 'laptop' && confirm("Press OK if you're sure of the details provided")) {
			if ($scope.updated_name != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].name = $scope.updated_name;
			if ($scope.updated_serial != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Serial = $scope.updated_serial;
			if ($scope.updated_wireless_mac != undefined){
				var mac_cool = true;
				angular.forEach($scope.updated_wireless_mac,function(value,key){
					if (!regex_mac_unit.test(value)) {
						mac_cool = false;
					}
				});
				if (mac_cool) {
					$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Wireless_MAC = $scope.updated_wireless_mac;
					} else {
					if($rootScope.notif_message_modal == undefined || $rootScope.notif_message_modal == '') {
						$scope.set_notif_modal("Invalid Wireless MAC Id");
						} else {
						var msg = $rootScope.notif_message_modal + ', Wireless MAC Id';
						$scope.set_notif_modal(msg);
					}
					$scope.dismiss_notif_modal(3000);
				}
			}
			if ($scope.updated_wired_mac != undefined){
				var mac_cool = true;
				angular.forEach($scope.updated_wired_mac,function(value,key){
					if (!regex_mac_unit.test(value)) {
						mac_cool = false;
					}
				});
				if (mac_cool) {
					$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Wired_MAC = $scope.updated_wired_mac;
					} else {
					if($rootScope.notif_message_modal == undefined || $rootScope.notif_message_modal == '') {
						$scope.set_notif_modal("Invalid Wired MAC Id");
						} else {
						var msg = $rootScope.notif_message_modal + ', Wired MAC Id';
						$scope.set_notif_modal(msg);
					}
					$scope.dismiss_notif_modal(3000);
				}
			}
			if ($scope.updated_manufacturer != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Manufacturer = $scope.updated_manufacturer;
			if ($scope.updated_model != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Model = $scope.updated_model;
			} else if (type == 'desktop' && confirm("Press OK if you're sure of the details provided")) {
			if ($scope.updated_name != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].name = $scope.updated_name;
			if ($scope.updated_serial != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Serial = $scope.updated_serial;
			if ($scope.updated_wireless_mac != undefined){
				var mac_cool = true;
				angular.forEach($scope.updated_wireless_mac,function(value,key){
					if (!regex_mac_unit.test(value)) {
						mac_cool = false;
					}
				});
				if (mac_cool) {
					$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Wireless_MAC = $scope.updated_wireless_mac;
					} else {
					if($rootScope.notif_message_modal == undefined || $rootScope.notif_message_modal == '') {
						$scope.set_notif_modal("Invalid Wireless MAC Id");
						} else {
						var msg = $rootScope.notif_message_modal + ', Wireless MAC Id';
						$scope.set_notif_modal(msg);
					}
					$scope.dismiss_notif_modal(3000);
				}
			}
			if ($scope.updated_wired_mac != undefined){
				var mac_cool = true;
				angular.forEach($scope.updated_wired_mac,function(value,key){
					if (!regex_mac_unit.test(value)) {
						mac_cool = false;
					}
				});
				if (mac_cool) {
					$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Wired_MAC = $scope.updated_wired_mac;
					} else {
					if($rootScope.notif_message_modal == undefined || $rootScope.notif_message_modal == '') {
						$scope.set_notif_modal("Invalid Wired MAC Id");
						} else {
						var msg = $rootScope.notif_message_modal + ', Wired MAC Id';
						$scope.set_notif_modal(msg);
					}
					$scope.dismiss_notif_modal(3000);
				}
			}
			if ($scope.updated_manufacturer != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Manufacturer = $scope.updated_manufacturer;
			if ($scope.updated_wireless_adapter_manufacturer != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Wireless_Adapter_Manufacturer = $scope.updated_wireless_adapter_manufacturer;
			if ($scope.updated_wireless_adapter_model != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Wireless_Adapter_Model = $scope.updated_wireless_adapter_model;
			if ($scope.updated_model != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Model = $scope.updated_model;
		}
		$http({
			url : 'update_data.php',
			method: 'get',
			params: {
				data: $rootScope.user_data_devices.data[index]
			}
			}).success(function(val){
		});
		$scope.evaluate_data_devices();
		$scope.empty_model_vars();
		
	};
	
	$scope.update_data = function(updated_data,index,type) {
		var regex_name = /^[a-zA-Z]+(\s[a-zA-Z]+)*$/;
		var regex_room_no = /^[0-9]{1,3}$/;
		if (type == "room_number" && !regex_room_no.test(updated_data)) {
			$scope.set_notif("We don't take such values! Please try again.");
			$scope.dismiss_notif(3000);
		}
		else if (updated_data == "") {
			$scope.set_notif("We don't take such values! Please try again.");
			$scope.dismiss_notif(3000);
		} else
		$http({
			url:  $rootScope.url + 'update_data.php',
			method: 'get',
			params:{
				index:index,
				data:updated_data
			}
			}).success(function(val) {
			if (val == true) {
				
				$scope.set_notif("Updated successfully");
				$scope.dismiss_notif(3000);
				
				if (index == 0) {
					$rootScope.user_data.data[0].value = updated_data;
					} else if (index == 1) {
					$rootScope.user_data.data[1].value = updated_data;
					} else if (index == 2) {
					$rootScope.user_data.data[2].value = updated_data;
					} else if (index == 3){ 
					$rootScope.user_data.data[3].value = updated_data;
					} else if (index == 4) {
					$rootScope.user_data.data[4].value = updated_data;
					} else if (index == 5) {
					$rootScope.user_data.data[5].value = updated_data;
					} else if (index == 6) {
					$rootScope.user_data.data[6].value = updated_data;
				}	
				
				$scope.evaluate_data();
				} else {
				$scope.set_notif("Nope! Couldn't update data! Please try again later.''");
				$scope.dismiss_notif(3000);
			}
		});
	};	
	
	$scope.read_text = function(index) {
		alert(JSON.stringify(devices[index]));
	};	
	
	$scope.add_device = function(type_of_device) {
		$http({
			url:  $rootScope.url + 'add_device.php',
			method: 'GET',
			params: {
				type:type_of_device
			}
			}).success(function(val) {
			// var currentPageTemplate = $route.current.templateUrl;
			// $templateCache.remove(currentPageTemplate);
			// var elem = angular.element(document.querySelector(".append_here"));
			// elem.append('<div class="added_device_box centered centered_row" data-toggle="modal" data-target="#fill_details_modal"><img src="assets/img/'+type_of_device+'.png"><span><b>'+type_of_device+'</b></span></div>');
			if(val.status == "success"){
				$route.reload();
			}
			else{
				alert("Could not add device! Sorry!");
			}
		});
	};
	
	$scope.delete_device = function(Serial,index){
		$http({
			url:  $rootScope.url + 'delete_device.php',
			method: 'GET',
			params: {
				serial: Serial
			}
			}).success(function(val) {
			// var currentPageTemplate = $route.current.templateUrl;
			// $templateCache.remove(currentPageTemplate);
			$route.reload();
		});
	};
	
	$scope.hover = function(){
		alert("asdfasd");
	};
	
	$scope.show_notification = function(entity,filled) {
		if (entity == "you") {
			// $scope.show_notification_panel = true;
			$scope.index_notification_leading= "You";
			$scope.index_notification = $rootScope.user_details_filled == true ? "Your details look good! Click below to view them.":"Woah! Looks like you've got some blank fields. Fill up your details to enable the devices tab.";
			$scope.index_notification_route_you = '#personal_details';
			// $scope.index_notification_label= 'View Personal Details';
			} else if (entity == "devices") {
			if (filled) {
				// $scope.show_notification_panel = true;
				$scope.index_notification_leading = "Devices";
				$scope.index_notification = "One can connect to KU-Internet from many devices. View your devices.";
				$scope.index_notification_route_devices = $rootScope.agreed_itpolicy == true ? '#devices':'#gotcha';
				// $scope.index_notification_label = 'View Your Devices';
				} else {
				$scope.set_notif("Please fill up your personal details completely");
				$scope.index_notification_route_devices = $rootScope.agreed_itpolicy == true ? '#devices':'#gotcha';
				$scope.dismiss_notif(2000);
			}
			} else if (entity == "usage") {
			if (filled) {
				// $scope.show_notification_panel = true;
				$scope.index_notification_leading = "Usage";
				$scope.index_notification = "Let's see how much of KU-Internet you've used up.";
				$scope.index_notification_route_usage = '#usage';
				// $scope.index_notification_label = 'View Your Internet Usage';
				} else {
				$scope.set_notif("Please fill up your personal details completely");
				$scope.index_notification_route_usage = '#usage';
				$scope.dismiss_notif(2000);
			}
			} else if (entity == "help") {
			// $scope.show_notification_panel = true;
			$scope.index_notification_leading = "netadmin@karunya.edu";
			$scope.index_notification = "Yep!, Drop your queries here.";
			// $scope.index_notification_route_usage = '#usage';
			// $scope.index_notification_label = 'View Your Internet Usage';
		}
		
	};
	
	$scope.please_wait = function() {
		$scope.secs = 5;
		$scope.button_disabled = "button_disabled";
		$rootScope.agreed_itpolicy = false;
		$scope.disable_agree = true;
		$interval(function(){
			$scope.secs = $scope.secs - 1;
			if($scope.secs == 0){
				$scope.disable_agree = false;
				$scope.button_disabled = "";
				$interval.cancel();
			}
		},1000,5);
	};
	
	$scope.agree_policy = function() {
		$rootScope.agreed_itpolicy = true;
		$location.path('/devices');
	};
	
	$scope.empty_model_vars = function(){
		$scope.updated_imei = undefined;
		$scope.updated_name = undefined;
		$scope.updated_serial = undefined;
		$scope.updated_manufacturer = undefined;
		$scope.updated_wired_mac = undefined;
		$scope.updated_wireless_mac = undefined;
		$scope.updated_wireless_adapter_manufacturer = undefined;
		$scope.updated_wireless_adapter_manufacturer = undefined;
		$scope.updated_wireless_adapter_model = undefined;
		$scope.updated_mobile_number = undefined;
		$scope.updated_model = undefined;
	};
	
})
// .directive('force_uppercase',function(){
// return {
// require: 'updated_wired_mac[0]',
// link: function(scope,element, attrs, modelCtrl){
// modelCtrl.$parsers.push(function(input){
// return input.toUpperCase();
// });
// }
// };
// })
.service('Session',function($rootScope,$http){
	this.create = function(credentials){
		this.username = credentials.username;
		this.password = credentials.password;
		$http({
			url: 'http://localhost/ctv/check.php',
			method: 'get',
			params:{
				username:credentials.username,
				password:credentials.password
			}
			}).success(function(val){
			if(val.status == true){
				$rootScope.username = val.username;
				$rootScope.password = val.password;
			}
			else{
				$rootScope.username = null;
				$rootScope.password = null;
			}
		});
	};
	this.destroy = function(){
		this.username = null;
		this.password = null;
		$rootScope.username = null;
		$rootScope.password = null;
	};
})
.config(function($routeProvider, $locationProvider) {
	$routeProvider
	.when('/personal_details', {
		templateUrl: 'app/personal_details.html',
		controller: 'wifiregcontroller'
	})
	.when('/index', {
		templateUrl: 'app/index.html',
		controller: 'wifiregcontroller'
	})
	.when('/gotcha', {
		templateUrl: 'app/gotcha.html',
		controller: 'wifiregcontroller'
	})
	.when('/usage', {
		templateUrl: 'app/usage.html',
		controller: 'wifiregcontroller'
	})
	.when('/help', {
		templateUrl: 'app/help.html',
		controller: 'wifiregcontroller'
	})
	.when('/devices', {
		templateUrl: 'app/devices.html',
		controller: 'wifiregcontroller'
	});
	// configure html5 to get links working on jsfiddle
	//$locationProvider.html5Mode(true);
});																	