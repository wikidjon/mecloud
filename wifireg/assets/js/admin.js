var app = angular.module('wifiregapp',['ngRoute'])
.constant('userDataArray',{	
	"data0": "$rootScope.user_data_devices.data[index].name",
	"data3": "Wireless_MAC"
})
.controller('wifiregcontroller',function($scope,Session,$window,$http,$location,$timeout,$rootScope,$interval,$route,userDataArray) {
	//Variable declarations
	$rootScope.show_search_elements = true;

	//Route change watching
	$rootScope.$on('$routeChangeStart',function(event, next, current){
		//Unauthorized users
		if(next.templateUrl === 'app/gotcha.html' && !$rootScope.user_details_filled){
			event.preventDefault();
		}
		else if(next.templateUrl === 'app/devices.html' && !$rootScope.agreed_itpolicy){
			event.preventDefault();
		}
		else if(next.templateUrl === 'app/usage.html' && !$rootScope.user_details_filled){
			event.preventDefault();
		}
		else if(next.templateUrl === 'app/gotcha.html' && $rootScope.user_details_filled && $rootScope.agreed_itpolicy){
			event.preventDefault();
		}
		
	});
	
	//Auto route
	$scope.auto_route = function(){
		$location.path('/index');
	}
	
	$scope.search = function(name){
		$rootScope.body_overflow = "add_overflow";
		
		$http({
			method: 'GET',
			url: 'admin/load_search_res.php',
			params: {
				name: $scope.name,
				reg: $scope.register_number,
				wmac: $scope.wireless_mac
			}
		}).success(function(val){
			if (val.status = "success") {
				$rootScope.user_data = val.data;
				$rootScope.show_result_table = true;
				$rootScope.show_search_elements = false;
				$rootScope.show_expand_button = true;
				$window.scrollTo(700,0);
				$rootScope.search_div = "search_div_decrease_height";
			}
		});
	};
	
	$scope.expand_search_box = function(){
		$rootScope.show_search_elements = true;
		$rootScope.search_div='';
		$rootScope.show_expand_button = false;
	};
	
	$scope.show_user_details = function(name){
		$rootScope.protag = {
			name: name
		};
		$location.path('/user_details');
	};
	
	$scope.load_protag = function(){
		$http({
			method: 'GET',
			url: 'admin/load_protag.php'
		}).success(function(val){
			if (val.status == "success") {
				$rootScope.protag_data = val.data.field;
			}
		});
	};
	
	$scope.dismiss_notif = function(delay) {
		$timeout(
		function() {
			$rootScope.error_notif_show = "";
			$rootScope.notif_message = "";
		},delay
		);
	};
	
	$scope.set_notif = function(msg) {
		$rootScope.error_notif_show = "error_notif_show";
		$rootScope.notif_message = msg;
	};
	
	$scope.dismiss_notif_modal = function(delay) {
		$timeout(
		function() {
			$rootScope.error_notif_show = "";
			$rootScope.notif_message_modal = "";
		},delay
		);
	};
	
	$scope.set_notif_modal = function(msg) {
		$rootScope.error_notif_show = "error_notif_show";
		$rootScope.notif_message_modal = msg;
	};
	
	$scope.view_device_details = function(type,data,index) {
		$rootScope.currently_editing_device = data;
		$rootScope.currently_editing_device_type = type;
		$rootScope.currently_editing_device_index = index;
	};
	
	$scope.set_for_edit_devices = function(data,index) {
		$scope.currently_editing = data;
		$scope.currently_editing_index = index;
	};
	
	$scope.update_data_devices = function(index,type) {
		var regex_name = /^[a-zA-Z^0-9_]+(\s[a-zA-Z^0-9]+)*$/;
		var regex_mac_unit = /^[a-fA-F^0-9]{2}$/;
		var regex_mobile= /^[0-9]{10}$/;
		if (type == 'phone' && confirm("Press OK if you're sure of the details provided")) {
			// angular.forEach([$scope.updated[0],$scope.updated[1],$scope.updated[2],$scope.updated[4],$scope.updated[5],$scope.updated[8],$scope.updated[9]],function(value,key){
			// if (value != undefined) {
			// $rootScope.user_data_devices.data[data].userDataArray.data3 = value;
			// }
			// });
			if ($scope.updated_name != undefined) {
				var mac_cool = true;
				angular.forEach($scope.updated_name,function(value,key){
					if (!regex_mac_unit.test(value)) {
						mac_cool = false;
					}
				});
				if (mac_cool) {
					$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].name = $scope.updated_name;
					} else {
					if($rootScope.notif_message_modal == undefined || $rootScope.notif_message_modal == '') {
						$scope.set_notif_modal("Invalid Name");
						} else {
						var msg = $rootScope.notif_message_modal + ', Name';
						$scope.set_notif_modal(msg);
					}
					$scope.dismiss_notif_modal(3000);
				}
			}
			if ($scope.updated_serial != undefined)
			
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Serial = $scope.updated_serial;
			if ($scope.updated_wireless_mac != undefined){
				var mac_cool = true;
				angular.forEach($scope.updated_wireless_mac,function(value,key){
					if (!regex_mac_unit.test(value)) {
						mac_cool = false;
					}
				});
				if (mac_cool) {
					$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Wireless_MAC = $scope.updated_wireless_mac;
					} else {
					if($rootScope.notif_message_modal == undefined || $rootScope.notif_message_modal == '') {
						$scope.set_notif_modal("Invalid Wireless Mac Id");
						} else {
						var msg = $rootScope.notif_message_modal + ', Wireless Mac Id';
						$scope.set_notif_modal(msg);
					}
					$scope.dismiss_notif_modal(3000);
				}
			}
			
			if ($scope.updated_manufacturer != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Manufacturer = $scope.updated_manufacturer;
			if ($scope.updated_model != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Model = $scope.updated_model;
			if ($scope.updated_imei != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].IMEI = $scope.updated_imei;
			if ($scope.updated_mobile_number != undefined){
				var mac_cool = true;
				if (!regex_mobile.test($scope.updated_mobile_number)) {
					mac_cool = false;
				}
				if (mac_cool) {
					$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Mobile_Number = $scope.updated_mobile_number;
					} else {
					if($rootScope.notif_message_modal == undefined || $rootScope.notif_message_modal == '') {
						$scope.set_notif_modal("Invalid Mobile Number");
						} else {
						var msg = $rootScope.notif_message_modal + ', Mobile Number';
						$scope.set_notif_modal(msg);
					}
					$scope.dismiss_notif_modal(3000);
				}
			}
			} else if (type == 'tablet' && confirm("Press OK if you're sure of the details provided")) {
			if ($scope.updated_name != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].name = $scope.updated_name;
			if ($scope.updated_serial != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Serial = $scope.updated_serial;
			if ($scope.updated_wireless_mac != undefined){
				var mac_cool = true;
				angular.forEach($scope.updated_wireless_mac,function(value,key){
					if (!regex_mac_unit.test(value)) {
						mac_cool = false;
					}
				});
				if (mac_cool) {
					$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Wireless_MAC = $scope.updated_wireless_mac;
					} else {
					if($rootScope.notif_message_modal == undefined || $rootScope.notif_message_modal == '') {
						$scope.set_notif_modal("Invalid Wireless MAC Id");
						} else {
						var msg = $rootScope.notif_message_modal + ', Wireless MAC Id';
						$scope.set_notif_modal(msg);
					}
					$scope.dismiss_notif_modal(3000);
				}
			}
			if ($scope.updated_manufacturer != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Manufacturer = $scope.updated_manufacturer;
			if ($scope.updated_model != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Model = $scope.updated_model;
			
			} else if (type == 'laptop' && confirm("Press OK if you're sure of the details provided")) {
			if ($scope.updated_name != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].name = $scope.updated_name;
			if ($scope.updated_serial != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Serial = $scope.updated_serial;
			if ($scope.updated_wireless_mac != undefined){
				var mac_cool = true;
				angular.forEach($scope.updated_wireless_mac,function(value,key){
					if (!regex_mac_unit.test(value)) {
						mac_cool = false;
					}
				});
				if (mac_cool) {
					$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Wireless_MAC = $scope.updated_wireless_mac;
					} else {
					if($rootScope.notif_message_modal == undefined || $rootScope.notif_message_modal == '') {
						$scope.set_notif_modal("Invalid Wireless MAC Id");
						} else {
						var msg = $rootScope.notif_message_modal + ', Wireless MAC Id';
						$scope.set_notif_modal(msg);
					}
					$scope.dismiss_notif_modal(3000);
				}
			}
			if ($scope.updated_wired_mac != undefined){
				var mac_cool = true;
				angular.forEach($scope.updated_wired_mac,function(value,key){
					if (!regex_mac_unit.test(value)) {
						mac_cool = false;
					}
				});
				if (mac_cool) {
					$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Wired_MAC = $scope.updated_wired_mac;
					} else {
					if($rootScope.notif_message_modal == undefined || $rootScope.notif_message_modal == '') {
						$scope.set_notif_modal("Invalid Wired MAC Id");
						} else {
						var msg = $rootScope.notif_message_modal + ', Wired MAC Id';
						$scope.set_notif_modal(msg);
					}
					$scope.dismiss_notif_modal(3000);
				}
			}
			if ($scope.updated_manufacturer != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Manufacturer = $scope.updated_manufacturer;
			if ($scope.updated_model != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Model = $scope.updated_model;
			} else if (type == 'desktop' && confirm("Press OK if you're sure of the details provided")) {
			if ($scope.updated_name != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].name = $scope.updated_name;
			if ($scope.updated_serial != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Serial = $scope.updated_serial;
			if ($scope.updated_wireless_mac != undefined){
				var mac_cool = true;
				angular.forEach($scope.updated_wireless_mac,function(value,key){
					if (!regex_mac_unit.test(value)) {
						mac_cool = false;
					}
				});
				if (mac_cool) {
					$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Wireless_MAC = $scope.updated_wireless_mac;
					} else {
					if($rootScope.notif_message_modal == undefined || $rootScope.notif_message_modal == '') {
						$scope.set_notif_modal("Invalid Wireless MAC Id");
						} else {
						var msg = $rootScope.notif_message_modal + ', Wireless MAC Id';
						$scope.set_notif_modal(msg);
					}
					$scope.dismiss_notif_modal(3000);
				}
			}
			if ($scope.updated_wired_mac != undefined){
				var mac_cool = true;
				angular.forEach($scope.updated_wired_mac,function(value,key){
					if (!regex_mac_unit.test(value)) {
						mac_cool = false;
					}
				});
				if (mac_cool) {
					$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Wired_MAC = $scope.updated_wired_mac;
					} else {
					if($rootScope.notif_message_modal == undefined || $rootScope.notif_message_modal == '') {
						$scope.set_notif_modal("Invalid Wired MAC Id");
						} else {
						var msg = $rootScope.notif_message_modal + ', Wired MAC Id';
						$scope.set_notif_modal(msg);
					}
					$scope.dismiss_notif_modal(3000);
				}
			}
			if ($scope.updated_manufacturer != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Manufacturer = $scope.updated_manufacturer;
			if ($scope.updated_wireless_adapter_manufacturer != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Wireless_Adapter_Manufacturer = $scope.updated_wireless_adapter_manufacturer;
			if ($scope.updated_wireless_adapter_model != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Wireless_Adapter_Model = $scope.updated_wireless_adapter_model;
			if ($scope.updated_model != undefined)
			$rootScope.user_data_devices.data[$rootScope.currently_editing_device_index].Model = $scope.updated_model;
		}
		$http({
			url : 'update_data.php',
			method: 'get',
			params: {
				data: $rootScope.user_data_devices.data[index]
			}
			}).success(function(val){
		});
		$scope.evaluate_data_devices();
		$scope.empty_model_vars();
		
	};
	
	$scope.update_data = function(updated_data,index,type) {
		var regex_name = /^[a-zA-Z]+(\s[a-zA-Z]+)*$/;
		var regex_room_no = /^[0-9]{1,3}$/;
		if (type == "room_number" && !regex_room_no.test(updated_data)) {
			$scope.set_notif("We don't take such values! Please try again.");
			$scope.dismiss_notif(3000);
		}
		else if (updated_data == "") {
			$scope.set_notif("We don't take such values! Please try again.");
			$scope.dismiss_notif(3000);
		} else
		$http({
			url:  $rootScope.url + 'update_data.php',
			method: 'get',
			params:{
				index:index,
				data:updated_data
			}
			}).success(function(val) {
			if (val == true) {
				
				$scope.set_notif("Updated successfully");
				$scope.dismiss_notif(3000);
				
				if (index == 0) {
					$rootScope.user_data.data[0].value = updated_data;
					} else if (index == 1) {
					$rootScope.user_data.data[1].value = updated_data;
					} else if (index == 2) {
					$rootScope.user_data.data[2].value = updated_data;
					} else if (index == 3){ 
					$rootScope.user_data.data[3].value = updated_data;
					} else if (index == 4) {
					$rootScope.user_data.data[4].value = updated_data;
					} else if (index == 5) {
					$rootScope.user_data.data[5].value = updated_data;
					} else if (index == 6) {
					$rootScope.user_data.data[6].value = updated_data;
				}	
				
				$scope.evaluate_data();
				} else {
				$scope.set_notif("Nope! Couldn't update data! Please try again later.''");
				$scope.dismiss_notif(3000);
			}
		});
	};	
	
	$scope.read_text = function(index) {
		alert(JSON.stringify(devices[index]));
	};	
	
	$scope.add_device = function(type_of_device) {
		$http({
			url:  $rootScope.url + 'add_device.php',
			method: 'GET',
			params: {
				type:type_of_device
			}
			}).success(function(val) {
			// var currentPageTemplate = $route.current.templateUrl;
			// $templateCache.remove(currentPageTemplate);
			// var elem = angular.element(document.querySelector(".append_here"));
			// elem.append('<div class="added_device_box centered centered_row" data-toggle="modal" data-target="#fill_details_modal"><img src="assets/img/'+type_of_device+'.png"><span><b>'+type_of_device+'</b></span></div>');
			if(val.status == "success"){
				$route.reload();
			}
			else{
				alert("Could not add device! Sorry!");
			}
		});
	};
	
	$scope.delete_device = function(Serial,index){
		$http({
			url:  $rootScope.url + 'delete_device.php',
			method: 'GET',
			params: {
				serial: Serial
			}
			}).success(function(val) {
			// var currentPageTemplate = $route.current.templateUrl;
			// $templateCache.remove(currentPageTemplate);
			$route.reload();
		});
	};
	
	$scope.hover = function(){
		alert("asdfasd");
	};
	
	$scope.show_notification = function(entity) {
		if (entity == "you") {
			$scope.index_notification_leading= "Users";
			$scope.index_notification = "List all the internet users of Karunya University. Man, Woman, Staff, Layman, Pauper......all under one roof!";
			$scope.index_notification_route_you = '#users';
			} else if (entity == "search") {
			$scope.index_notification_leading = "Search";
			$scope.index_notification = "Find users.";
			$scope.index_notification_route_search= '#search';
		}
	};
	
	$scope.please_wait = function() {
		$scope.secs = 5;
		$scope.button_disabled = "button_disabled";
		$rootScope.agreed_itpolicy = false;
		$scope.disable_agree = true;
		$interval(function(){
			$scope.secs = $scope.secs - 1;
			if($scope.secs == 0){
				$scope.disable_agree = false;
				$scope.button_disabled = "";
				$interval.cancel();
			}
		},1000,5);
	};
	
	$scope.agree_policy = function() {
		$rootScope.agreed_itpolicy = true;
		$location.path('/devices');
	};
	
	$scope.empty_model_vars = function(){
		$scope.updated_imei = undefined;
		$scope.updated_name = undefined;
		$scope.updated_serial = undefined;
		$scope.updated_manufacturer = undefined;
		$scope.updated_wired_mac = undefined;
		$scope.updated_wireless_mac = undefined;
		$scope.updated_wireless_adapter_manufacturer = undefined;
		$scope.updated_wireless_adapter_manufacturer = undefined;
		$scope.updated_wireless_adapter_model = undefined;
		$scope.updated_mobile_number = undefined;
		$scope.updated_model = undefined;
	};
	
})
// .directive('force_uppercase',function(){
// return {
// require: 'updated_wired_mac[0]',
// link: function(scope,element, attrs, modelCtrl){
// modelCtrl.$parsers.push(function(input){
// return input.toUpperCase();
// });
// }
// };
// })
.service('Session',function($rootScope,$http){
	this.create = function(credentials){
		this.username = credentials.username;
		this.password = credentials.password;
		$http({
			url: 'http://localhost/ctv/check.php',
			method: 'get',
			params:{
				username:credentials.username,
				password:credentials.password
			}
			}).success(function(val){
			if(val.status == true){
				$rootScope.username = val.username;
				$rootScope.password = val.password;
			}
			else{
				$rootScope.username = null;
				$rootScope.password = null;
			}
		});
	};
	this.destroy = function(){
		this.username = null;
		this.password = null;
		$rootScope.username = null;
		$rootScope.password = null;
	};
})
.config(function($routeProvider, $locationProvider) {
	$routeProvider
	.when('/index', {
		templateUrl: 'admin/index.html',
		controller: 'wifiregcontroller'
	})
	.when('/user_details', {
		templateUrl: 'admin/user_details.html',
		controller: 'wifiregcontroller'
	})
	.when('/search', {
		templateUrl: 'admin/search.html',
		controller: 'wifiregcontroller'
	});
	// configure html5 to get links working on jsfiddle
	//$locationProvider.html5Mode(true);
});																	