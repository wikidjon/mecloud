var app = angular.module('wifiregapp',['ngRoute'])
.controller('wifiregcontroller',function($scope,Session,$window,$http,$location,$interval,$rootScope){
	//Variable declarations
	$scope.msg = "Awdaw";
	$scope.data = [];
	$scope.data[0] = "Jonathan";
	$scope.data[1] = "UR12CS100";
	$scope.data[2] = "CSE";
	$scope.data_type = [];
	$scope.data_type[0] = "Name";
	$scope.data_type[1] = "Register Number";
	$scope.data_type[2] = "Department";
	
	$scope.disable_edit = [];
	$scope.disable_edit[0] = false;
	$scope.disable_edit[1] = true;
	$scope.disable_edit[2] = false;
	
	$scope.device_count = 0;
	$scope.laptops = 0;
	$scope.tablets = 0;
	$scope.phones = 0;
	$scope.desktop = 0;
	
	//Auto route
	// $location.path('/index');
	
	$scope.login = function(){
		
		$http({
			url: 'http://localhost/ctv/check.php',
			method: 'get',
			params:{
				username:credentials.username,
				password:credentials.password
			}
			}).success(function(val){
			if(val.status == true){
				$scope.username = val.username;
				$scope.password = val.password;
				$scope.wrong_cred = false;
				$scope.welcome_msg = val.admin == true ? "Admin Menu":"User Menu";
				//Specify whether user is admin or normal user
				$rootScope.admin = val.admin == 0 ? false:true;
				$scope.admin = val.admin == 0 ? false:true;
			}
			else{
				$scope.wrong_cred = true;
			}
		});
	};
	
	$scope.destroy_session = function(){
		$scope.username = null;
		$scope.credentials.username = null;
		$scope.credentials.password = null;
		$scope.password = null;
		$location.path('index');
	};
	
	$scope.set_for_edit = function(data,index){
		$scope.currently_editing = data;
		$scope.currently_editing_index = index;
	};
	
	$scope.update_data = function(updated_data){
		$scope.data[$scope.currently_editing_index] = updated_data;
		$http({
			url: 'http://localhost/wifireg/update_data.php',
			method: 'get',
			params:{
				index:$scope.currently_editing_index,
				data:updated_data
			}
			}).success(function(val){
				if(val == true){
					alert("Done");
				}
				else{
					alert("Nope");
				}
		});
	};	
	
	$scope.read_text = function(index){
		alert(JSON.stringify(devices[index]));
	};	
	
	$scope.add_device = function(device){
		var elem = angular.element(document.querySelector(".append_here"));
		elem.append('<div class="added_device_box centered centered_row" data-toggle="modal" data-target="#fill_details_modal"><img src="assets/img/'+device+'.png"><span><b>'+device+'</b></span></div>');
	};
	
	$scope.show_notification = function(entity){
		if(entity == "laptop"){
			$scope.show_notification_panel = true;
			$scope.index_notification = "Woah! Looks like you've got some blank fields. Click below to go to page.'";
		}
	}
})
.service('Session',function($rootScope,$http){
	this.create = function(credentials){
		this.username = credentials.username;
		this.password = credentials.password;
		$http({
			url: 'http://localhost/ctv/check.php',
			method: 'get',
			params:{
				username:credentials.username,
				password:credentials.password
			}
			}).success(function(val){
			if(val.status == true){
				// alert("Hey");
				$rootScope.username = val.username;
				$rootScope.password = val.password;
			}
			else{
				$rootScope.username = null;
				$rootScope.password = null;
			}
		});
	};
	this.destroy = function(){
		this.username = null;
		this.password = null;
		$rootScope.username = null;
		$rootScope.password = null;
	};
})
.config(function($routeProvider, $locationProvider) {
	$routeProvider
	.when('/personal_details', {
		templateUrl: 'app/personal_details.html',
		controller: 'wifiregcontroller'
	})
	.when('/index', {
		templateUrl: 'app/index.html',
		controller: 'wifiregcontroller'
	})
	.when('/devices', {
		templateUrl: 'app/devices.html',
		controller: 'wifiregcontroller'
	});
	// configure html5 to get links working on jsfiddle
	$locationProvider.html5Mode(true);
});