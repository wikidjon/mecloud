<?php
	//Path to simple dom parser
	include('simple_html_dom.php');
	//Urls for extraction
	$html = file_get_html('https://photography.nationalgeographic.com/photography/photo-of-the-day/');
	$bin = file_get_html('http://apod.nasa.gov/apod/astropix.html');
	
	//URL array
	$url = array();
	
	//Extract Nat Geo's daily wallpaper's src
	foreach ($html->find('div') as $element) {
		if($element->class == "primary_photo")
			foreach ($element->find('img') as $img) {
				array_push($url,$img->src); 
			}
	}
	
	//Extract Bing's' daily wallpaper's src
	foreach ($bin->find('img') as $element) {
		array_push($url,$element->src);
	}
	
	// echo '"'.$url[0].'",';
	// echo '"http://apod.nasa.gov/apod/'.$url[1].'"';
?>