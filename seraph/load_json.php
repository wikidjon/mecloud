<?php
	$output = '{
    "url": "https://photography.nationalgeographic.com/photography/photo-of-the-day/",
    
    "src": "assets/img/coffee.jpg",
    "description": "Pope Francis rides in a popemobile at a general audience in St. Peter\u2019s Square in this National Geographic Photo of the Day.",
    "title": "Pope Francis Image, Vatican City",
    "credits": "Reuters"
}';

	echo $output;
?>