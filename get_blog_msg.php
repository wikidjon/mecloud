<?php
	include 'db_connect.php';
	
	header("Access-Control-Allow-Origin: *");
	ini_set('date.timezone', 'Asia/Kolkata');
	
	if ($_GET['type'] == 'get_articles') {
		
		$stmt = $conn->prepare('select * from blog');
		
		$stmt->execute();
		if($stmt){
			$output = array();
			while($data = $stmt->fetch()){
				$output[] = $data;
			}
			echo json_encode($output);
		}
		else
		{
			echo false;
		}
	}
?>