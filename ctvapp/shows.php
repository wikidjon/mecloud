<?php
	
	include 'db_connect.php';
	ini_set('date.timezone', 'Asia/Kolkata');
	
	$type = $_GET['type'];
	
	if($type == "request_show"){
		
		$show = $_GET['show'];
		$from = $_GET['from'];
		$now = date('Y-m-d H:i:s');
		$stmt = $conn->prepare('insert into show_requests(`timestamp`,`show_name`,`from_user`) values(:timestamp,:show,:from)');
		
		$stmt ->bindParam(':timestamp',$now,PDO::PARAM_STR,100);
		$stmt ->bindParam(':show',$show,PDO::PARAM_STR,1000);
		$stmt ->bindParam(':from',$from,PDO::PARAM_STR,100);
		
		$stmt->execute();
		if($stmt){
			echo true;
		}
		else
		{
			echo false;
		}
	}
	else if($type == "load_requests"){
		
		$from = $_GET['from'];
		$admin = $_GET['admin'];
		if($admin == 'true'){
			$stmt = $conn->prepare('select * from show_requests');
		}
		else{
			$stmt = $conn->prepare('select * from show_requests where from_user = :from');
		}
		
		if($admin == 'false'){
			$stmt ->bindParam(':from',$from,PDO::PARAM_STR,100);
		}
		
		$stmt->execute();
		
		if($stmt){
			$output = array();
			while($data = $stmt->fetch()){
				$output[] = $data;
				// $output[] = array( "data" => $data);
				// echo json_encode($data);
			}
			echo json_encode($output);
		}
		else
		{
			echo false;
		}
	}
	else if($type == "approve_requests"){
		
		$show = $_GET['show'];
		$from = $_GET['from'];
		$approve = $_GET['approve'];
		
		$stmt = $conn->prepare('update show_requests set approve = :approve where from_user = :from AND show_name = :show');
		
		$stmt ->bindParam(':approve',$approve,PDO::PARAM_STR,1000);
		$stmt ->bindParam(':from',$from,PDO::PARAM_STR,1000);
		$stmt ->bindParam(':show',$show,PDO::PARAM_STR,1000);
		
		$stmt->execute();
		
		if($stmt){
			$output = array(
				"status" => true,
				"value" => $approve
			);
			
			echo json_encode($output); 
		}
		else
		{
			$output = array(
				"status" => false
			);
			
			echo json_encode($output);
		}
	} else if ($type == "flag_requests"){
		
		$show = $_GET['show'];
		$from = $_GET['from'];
		$flag = $_GET['flag'];
		
		$stmt = $conn->prepare('update show_requests set flag = :flag where from_user = :from AND show_name = :show');
		
		$stmt ->bindParam(':flag',$flag,PDO::PARAM_STR,1000);
		$stmt ->bindParam(':from',$from,PDO::PARAM_STR,1000);
		$stmt ->bindParam(':show',$show,PDO::PARAM_STR,1000);
		
		$stmt->execute();
		
		if($stmt){
			$output = array(
				"status" => true,
				"value" => $flag
			);
			
			echo json_encode($output); 
		}
		else
		{
			$output = array(
				"status" => false
			);
			
			echo json_encode($output);
		}
	} else if ($type == "delete_requests"){
		
		$show = $_GET['show'];
		$from = $_GET['from'];
		
		$stmt = $conn->prepare('delete from show_requests where from_user = :from AND show_name = :show');
		
		$stmt ->bindParam(':from',$from,PDO::PARAM_STR,1000);
		$stmt ->bindParam(':show',$show,PDO::PARAM_STR,1000);
		
		$stmt->execute();
		
		if($stmt){
			$output = array(
				"status" => true,
				"value" => "deleted"
			);
			
			echo json_encode($output); 
		}
		else
		{
			$output = array(
				"status" => false
			);
			
			echo json_encode($output);
		}
	}
?>