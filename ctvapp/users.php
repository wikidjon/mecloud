<?php
	include 'db_connect.php';
	
	header("Access-Control-Allow-Origin: *");
	
	if($_GET['type'] == 'login_or_create') {
		
		$username = $_GET['username'];
		$seraph_id = $_GET['seraph_id'];
		
		$check = $conn->prepare('select * from users where username = :username');
		$query = $conn->prepare('INSERT INTO `users`(`username`,`seraph_id`) VALUES (:username,:seraph_id)');
		
		$check->bindParam(':username',$username,PDO::PARAM_STR,100);
		$query->bindParam(':username',$username,PDO::PARAM_STR,100);
		$query->bindParam(':seraph_id',$seraph_id,PDO::PARAM_STR,100);
		
		$check->execute();
		$res = $check->fetch();
		
		if($res)
		{
			$admin = $res['admin'] == "1" ? true:false;
			$url = $admin == true ? 'adimin.php':'index.php';
			$output = array(
			"status" => "exists",
			"id" => $res['id'],
			"theme" => $res['theme'],
			"avatar" => $res['avatar'],
			"admin" => $admin,
			'url' => $url
			);
			
			echo json_encode($output);
		}
		else{
			
			$query->execute();
			
			if($query) {
				$res = $query->fetch();
				//Unique session id
				$output = array(
				"status" => "created"
				);
				echo json_encode($output);
				
				} else {
				$output = array(
				"status" => "insert_error"
				);
				
				echo json_encode($output);
			}
		}
	} 
	else if($_GET['type'] == 'make_admin') {
	
		$seraph_id = $_GET['seraph_id'];
		
		$query = $conn->prepare('update users set admin = "1" where seraph_id = :seraph_id');
		
		$query->bindParam(':seraph_id',$seraph_id,PDO::PARAM_STR,100);
		
		$query->execute();
		
		if($query)
		{
			$output = array(
				"status" => "converted_to_admin"
			);
			
			echo json_encode($output);
		}
		else{
			$output = array(
				"status" => "convert_error"
			);
			
			echo json_encode($output);
		}
	} else if($_GET['type'] == 'make_normal') {
	
		$seraph_id = $_GET['seraph_id'];
		
		$query = $conn->prepare('update users set admin = "0" where seraph_id = :seraph_id');
		
		$query->bindParam(':seraph_id',$seraph_id,PDO::PARAM_STR,100);
		
		$query->execute();
		
		if($query)
		{
			$output = array(
				"status" => "converted_to_normal"
			);
			
			echo json_encode($output);
		}
		else{
			$output = array(
				"status" => "convert_error"
			);
			
			echo json_encode($output);
		}
	}
?>