<!DOCTYPE html>
<html lang="en" ng-app="ctvapp" ng-controller="ctvapp_controller" ng-init="to_home()">
	<head>
		<title>Campus TV</title>
		<link rel="shortcut icon" href="assets/img/s_icon.png" />
	 	
		<!--Fetching CSS and JS files-->
		<script src="assets/js/angular.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/admin.js"></script>
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/ctvapp.css">
		
	</head>
	<body>
		<!--Start of seraph app-->
		<div class="error_notif centered" ng-class="error_notif_show">
			<h6>{{notif_message}}</h6>
		</div>
		
		<div class="container-fluid nopadding centered">
			<form ng-submit="make_admin()">
				<input type="text" ng-model="to_be_admin">
				<button>Go</button>
			</form>
		</div>
		
	<script>
	</script>
	<!--End of seraph app-->
	</body>
	</html>																										