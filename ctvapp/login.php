<!DOCTYPE html>
<html lang="en" ng-app="seraph_auth_app" ng-controller="seraph_auth_app_controller" ng-init="load_json()">
	<head>
		<title>Seraph Authentication</title>
		<link rel="shortcut icon" href="https://static.karunya.edu/seraph/img/s_icon.png"/>
	 	
		<!--Fetching CSS and JS files-->
		<script src="assets/js/jquery-2.1.3.min.js"></script>
		<script src="assets/js/angular.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/lfile_storage.min.js"></script>
		<script src="assets/js/jquery.backstretch.min.js"></script>
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/seraph-login-v2.min.css">
		
		<script type="text/javascript">
			$(document).ready(
			function()
			{
				//Centering Login Container
				$(".login_container").css({
					'margin-top':($(window).height()/2) - $(".login_container").height()/2 - 30
				});
				
				//To switch between login tab and intranet tab
				$(".header,.back_to_login").click(function() {
					var ch = $(".login_tab").css('opacity');
					if (ch == 1) {
						$(".local_tab").css({
							'opacity':'1',
							'display':'block'
						});
						$(".login_tab").css({
							'opacity':'0',
							'display':'none'
						});
						} else{ 
						$(".local_tab").css({
							'opacity':'0',
							'display':'none'
						});
						$(".login_tab").css({
							'opacity':'1',
							'display':'block'
						});
					}
				});
			});
		</script>
		
	</head>
	<body>
		<!--Start of seraph app-->
		<div class="error_notif centered" ng-class="error_notif_show">
			<h6>{{notif_message}}</h6>
		</div>
		
		<div class="footer_popup">
			<div ng-if="info">
				<h4 ng-if="(photo.title).length < 30">{{photo.title}}</h4>
				<div ng-if="(photo.title).length > 30" class="title_class">
					<h4>{{photo.title.substr(0,70)}}<a href="{{photo.url}}" target="_blank">...more</a></h4>				
				</div>
				<hr>
				Credits: {{photo.credits}}<br>
				<span ng-if="(photo.description).length < 30">{{photo.description}}</span>
				<div class="description_class">
					<span ng-if="(photo.description).length > 30">{{photo.description.substr(0,300)}}</span>
					<a href="{{photo.url}}" target="_blank">...more</a><br>
				</div>
			</div>
		</div>
		
		<div class="footer flex_end centered_row">
			<a href="">Reset Password</a>
			<div class="img_info centered" ng-click="toggle_info()">
				<img src="https://static.karunya.edu/seraph/img/i.png" />
			</div>
		</div>
		
		<div class="container-fluid centered_horizontal centered_col" {{isDisabled}}>
			
			<div class="login_container centered_horizontal centered_col">
				<div class="login_tab">
					<div class="logo centered centered_col">
						<img src="https://static.karunya.edu/seraph/img/seraph_shadow.png" />
					</div>
					
					<div class="user_img centered">
						<img src="https://static.karunya.edu/seraph/img/user_round.png">
					</div>
					
					<span class="logging_in_user_greet centered">{{greet}}</span>
					
					<div class="input_holder">
						<form ng-submit="auth_username()">
							<div class="centered centered_col username_auth">
								<input type="text" ng-model="username" class="username_field" maxlength="10" placeholder="Seraph ID">
								<input type="button" ng-click="auth_username()" value="login">
							</div>
						</form>
						<form ng-submit="auth_password()">
							<div class="centered centered_col password_auth">
								<div class="logging_in_user centered">{{username}}</div>
								<input type="password" name = "password" ng-model="password" class="password_field" placeholder="Password">
								<button class="">Login</button>
								<a href="" ng-click="reset()" class="help">Sign in as a different user</a>
							</div>
						</form>
						<div class="header centered" ng-click="toggle_links()">
							<div class="img_links centered">
								<img src="https://static.karunya.edu/seraph/img/menu.png"/>
							</div>
						</div>
					</div>
				</div>
				<div class="local_tab">
					<h3 class="centered ">Intranet Sites</h3>
					<a href=""><div class="local_tab_links">
						<span>Intranet Portal</span>
					</div></a>
					<hr/>
					<a href=""><div class="local_tab_links">
						<span>MIT Open Courseware</span>
					</div></a>
					<a href=""><div class="local_tab_links">
						<span>Digital Library</span>
					</div></a>
					<a href=""><div class="local_tab_links">
						<span>IT Services</span>
					</div></a>
					<a href=""><div class="local_tab_links">
						<span>FTP Server (Software Download)</span>
					</div></a>
					<hr/>
					<a href=""><div class="local_tab_links">
						<span>Karunya Forums</span>
					</div></a>
					<a href=""><div class="local_tab_links">
						<span>Campus TV</span>
					</div></a>
					<a href=""><div class="local_tab_links">
						<span>Campus Radio</span>
					</div></a>
					<div class="local_tab_links back_to_login centered">
						<a href=""><span>Back to login</span></a>
					</div>
					
				</div>
			</div>
		</div>
		
		<script>
			var photo_url = '';
			var app = angular.module('seraph_auth_app',['LocalStorageModule'])
			.config(function($logProvider){
				//$logProvider.debugEnabled(false);
			})
			.controller('seraph_auth_app_controller',function($scope,$location,localStorageService,$window,$rootScope,$timeout,$http,$log){
				//Initializations
				$(".username_field").focus();
				$(".user_img img").css({'opacity':'0.8'});
				$scope.user = "sdfasd";
				$scope.greet = "Please type your Seraph ID";
				$scope.info = true;
				$scope.links = true;
				// c$scope.disableApp();
				$scope.photo = {
					title: '',
					desc: '',
					credits: '',
					img: '',
					url: ''
				};
				
				$scope.load_json = function() {
					$http({
						url: 'https://static.karunya.edu/seraph/img/bg/?get=json',
						method: 'get'
						}).success(function(val) {
						$scope.photo = val;
						$.backstretch(val.src);
					});
				};
				
				$scope.auth_username = function() {
					$(".login_container").removeClass('shake');
					var regex_username = /^[a-zA-Z0-9]{1,10}$/;
					
					if ($scope.username=='' || $scope.username == undefined) {
						
						$(".login_container").addClass("shake");
						$scope.set_notif("We don't take null values!");
						$scope.dismiss_notif(3000);
						
						} else if (regex_username.test($scope.username)) {
						
						$scope.greet = "Please type your password";
						$(".user_img img").css({'opacity':'1'});
						$(".username_auth").css({
							'opacity':'0',
							'left':'-200px'
						});
						$(".password_auth").css({
							'opacity':'1',
							'left':'0px'
						});
						$(".login_container").css({
							'height':'520px'
						});
						$(".input_holder").css({
							'height':'220px'
						});
						$(".password_field").focus();
						$(".header").css({
							'bottom':'-210px'
						});
						
						} else if (!regex_username.test($scope.username)) {
						$(".login_container").addClass("shake");
						$scope.set_notif("Uhun! Invalid Seraph ID!");
						$scope.dismiss_notif(3000);
						
					}
				};
				
				$scope.auth_password = function() {
					$(".login_container").removeClass('shake');
					
					if ($scope.password == '' || $scope.password == undefined) {
						$(".login_container").addClass("shake");
						$scope.set_notif("We don't take null values!");
						$scope.dismiss_notif(3000);
						} else {
						
						$http({
							url: 'zion.php',
							method: 'GET',
							params: {
								username: $scope.username,
								password: $scope.password
							}
						})
						.success(function(val) {
							if(val.status)
							{
								localStorageService.cookie.set('username',val.name);
								localStorageService.cookie.set('seraph_id',val.username);
								$window.location.href = 'index.php';
							}
							else {
								$(".login_container").addClass("shake");
								$scope.set_notif("Invalid Credentials!");
								$scope.dismiss_notif(3000);
							}
						});
					}
				};
				
				$scope.reset = function() {
					$(".user_img img").css({'opacity':'1'});
					$scope.username = '';
					$scope.password = '';
					$(".username_auth").css({
						'opacity':'1',
						'left':'0px'
					});
					$(".password_auth").css({
						'opacity':'0',
						'left':'200px'
					});
					$(".login_container").css({
						'height':'460px'
					});
					$(".input_holder").css({
						'height':'150px'
					});
					$(".user_img img").css({'opacity':'0.8'});
					$(".username_field").focus();	
					$(".header").css({
						'bottom':'-150px'
					});
				};
				
				$scope.set_notif = function(msg) {
					$rootScope.error_notif_show = "error_notif_show";
					$rootScope.notif_message = msg;
				};
				
				$scope.dismiss_notif= function(delay) {
					$timeout(
					function() {
						$rootScope.error_notif_show = "";
						$rootScope.notif_message = "";
					},delay
					);
				};
				
				$scope.disableApp = function() {
					$scope.set_notif("Sorry! Something went wrong and we're trying to fix it!");
					$scope.dismiss_notif(100000);
					$scope.isDisabled = 'disabled';
				};
				
				$scope.toggle_info = function() {
					var ch = $(".footer_popup").css('opacity');
					if (ch == 0) {
						$(".footer_popup").css({
							'opacity':'1',
							'right':'15px'
						});
						} else {
						$(".footer_popup").css({
							'opacity':'0',
							'right':'0px'
						});
					}
				};
				$scope.toggle_links = function() {
					
				};
			})
			.config(function(localStorageServiceProvider) {
				localStorageServiceProvider
				.setPrefix('ctv')
				.setStorageType('localStorage')
				.setStorageCookie(365, '/');
				
				// configure html5 to get links working on jsfiddle
				// $locationProvider.html5Mode(true);
			});
		</script>
		<!--End of seraph app-->
	</body>
</html>																																				