<!DOCTYPE html>
<html lang="en" ng-app="ctvapp" ng-controller="ctvapp_controller" ng-init="to_home()">
	<head>
		<title>Campus TV</title>
		<link rel="shortcut icon" href="assets/img/s_icon.png" />
	 	
		<!--Fetching CSS and JS files-->
		<script src="assets/js/jquery-2.1.3.min.js"></script>
		<script src="assets/js/angular.min.js"></script>
		<script src="assets/js/angular-route.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/jquery.backstretch.min.js"></script>
		<script src="assets/js/lfile_storage.min.js"></script>
		<script src="assets/js/admin.js"></script>
		<script src="assets/js/video.js"></script>
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/ctvapp.css">
		<link rel="stylesheet" href="assets/css/video-js.css">
		<link rel="stylesheet" href="assets/css/videojs-sublime-skin.min.css">
		
		<script type="text/javascript">
			var _paq = _paq || [];
			_paq.push(["trackPageView"]);
			_paq.push(["enableLinkTracking"]);
			
			(function() {
				var u=(("https:" == document.location.protocol) ? "https" : "http") + "://analytics.karunya.edu/";
				_paq.push(["setTrackerUrl", u+"piwik.php"]);
				_paq.push(["setSiteId", "3"]);
				var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
				g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
			})();
		</script>
		
		<script type="text/javascript">
			$(document).ready(
			function()
			{
				$(document).ready(
			function()
			{
				$("div").click(function(event){
					if((event.target.className).search("login_tab") != -1){
						if($(".drop_down").css('display') == 'none') 
						{
							$(".drop_down").addClass('show_drop_down');
						}
						else 
						{
							$(".drop_down").removeClass('show_drop_down');
						}
					} 
					else 
					{
						$(".drop_down").removeClass('show_drop_down');
					}
				});
				
				$("div").keyup(function(e){
					if(e.keyCode == 27){
						$(".drop_down").removeClass('show_drop_down');
					} 
				});
			});
			});
			//angular.element(document.getElementsByTagName('head')).append(angular.element('<base href="' + window.location.pathname + '" />'));
		</script>
		
	</head>
	<body>
		<!--Start of seraph app-->
		<div class="error_notif centered" ng-class="error_notif_show">
			<h6>{{notif_message}}</h6>
		</div>
		
		<div class="container-fluid nopadding">
			<div class="header centered_vertical blur">
				<!--Branding-->
				<div class="header_lefter col-sm-6 flex_start nopadding">
					<!--span class="heading">CampusTV</span-->
					<img src="assets/img/ctv.png" class="logo">
				</div>
				<!--User Login/Logout section-->
				<div class="header_righter col-sm-6 flex_end nopadding">
					<div class="user_tab centered centered_row">
						
						<a href=""><div ng-click="login_function()" ng-if="login_tab" class="login_tab centered centered_row">	
							<img ng-src="{{avatar_url}}">
							{{login_text}}
						</div></a>
						 
						<!--Popup-->
						<div class="drop_down centered centered_row hide_drop_down">
							<div class="user_settings centered centered_row">
								<div class="img_part">
									<img ng-src="{{avatar_url}}">
								</div>
								
								<div class="text_part centered_col">
									<h4>{{login_text}}</h4>
									<a href="#avatar">Change avatar</a>
									<br/>
								</div>
							</div>
							<div class="logout_tab centered_vertical flex_end">
								<button ng-click="logout()" class="request_button">Logout</button>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<!--Nav Menu-->
			<div class="menu centered blur">
				<div class="login">
				</div>
				<div class="navs centered">
					<a href="#home">Home</a>
					<a href="#schedule">Schedule</a>
					<a href="#request">Request Shows</a>
					<a href="#help">Help</a>
				</div>
				<div class="news">
					
				</div>
			</div>
			
			<div class="route centered center_fix centered_col" ng-view>
			</div>
			
		</div>
		
	<script>
	</script>
	<!--End of seraph app-->
	</body>
	</html>																										