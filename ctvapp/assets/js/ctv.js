var app = angular.module('ctvapp',['ngRoute','LocalStorageModule'])
.run(function($http,$rootScope,localStorageService,$location,$window){
	//Initializations
	$rootScope.login_tab = true;
	$rootScope.drop_down = false;
	
	if(localStorageService.cookie.get('username') == null) {
		$rootScope.login_text = "Login";
		$rootScope.avatar_url = 'assets/img/user_round.png';
		$rootScope.isLoggedIn = false;
		$location.path('home');
		
		} else {
		$http({
			url: 'users.php',
			method: 'get',
			params: {
				username: localStorageService.cookie.get('username'),
				seraph_id: localStorageService.cookie.get('seraph_id'),
				type: 'login_or_create'
			}
			}).success(function(val){
			
			if (val.status == "exists") {
				$rootScope.user_details = val;
				$rootScope.isLoggedIn = true;
				$rootScope.login_text = localStorageService.cookie.get('username');
				$rootScope.avatar_url = 'assets/img/avatars/' + val.avatar + '.png';
				localStorageService.cookie.set('avatar_url',$rootScope.avatar_url);
				if(val.admin){
					$window.location.href = 'adimin.php';
				} 
				else 
				{
					// $window.location.href = 'index.php';
					$location.path('home');
				}
			} 
			else if(val.status == 'created') {
				
				$rootScope.isLoggedIn = true;
				$rootScope.login_text = localStorageService.cookie.get('username');
				$location.path('create_profile');
				
				} else if (val.status == 'insert_error') {
				
				$scope.set_notif("Error creating profile!");
				$scope.dismiss_notif(3000);
				$location.path('home');
				
			}
		});
	}
})
.controller('ctvapp_controller',function($route,$scope,$location,$window,$rootScope,$timeout,$http,$interval,localStorageService){
	
	$rootScope.$on('$routeChangeStart',function(event, next, current){
		//Unauthorized users
		if(next.templateUrl === 'app/request.html' && !$rootScope.isLoggedIn){
			$scope.set_notif("Please log in to access that tab!");
			$scope.dismiss_notif(3000);
			event.preventDefault();
		} 
		else if(next.templateUrl === 'app/home.html'){
			if(current.templateUrl === 'app/request.html' || current.templateUrl === 'app/help.html' || current.templateUrl === 'app/avatar.html' || current.templateUrl === 'app/create_profile.html') 
			{
				$window.location.href = 'index.php';
			}
		}
	});
	
	//Interval functions
	//A function that runs every 1 second when the page is open to check for now playing video
	$scope.get_now_playing = function(){
		$http({
			url: 'get.php',
			method: 'GET',
			params: {
				type: 'get_now_playing'
			}
		}).success(function(val)
		{
			if (val != "no_video_playing") {
				$rootScope.now_playing = val;
				} else {
				$rootScope.now_playing = null;
			}
		});
	};
	
	//PIWIK daily count(action)
	$scope.visits = 'unavailable';
	$http({
		url: 'http://analytics.karunya.edu/?module=API&method=Live.getCounters&format=json&token_auth=07a6ebb37779915c4b314d0c5b689649&urls=ctv.karunya.edu&idSite=3&period=day&date=last1&lastMinutes=1440',
		method: 'get'
	}).success(function(val){
		$scope.visits = val.visits;
	});
		
	$interval(function(){
		$http({
			url: 'http://analytics.karunya.edu/?module=API&method=Live.getCounters&format=json&token_auth=07a6ebb37779915c4b314d0c5b689649&urls=ctv.karunya.edu&idSite=3&period=day&date=last1&lastMinutes=1440',
			method: 'get'
		}).success(function(val){
			$scope.visits = val.visits;
		});
	},60000);
	//Auto route
	$scope.to_home = function() {
		$location.path('home');
	};
	
	$scope.login_function = function(){
		if(localStorageService.cookie.get('username') == null) {
			$window.location.href = 'login.php';
			} else {
			// $scope.login_tab = false;
			$scope.drop_down = $scope.drop_down == true ? false:true;
		}
	};
	
	$scope.logout = function() {
		localStorageService.cookie.set('username',null);
		localStorageService.cookie.set('password',null);
		$window.location.href = 'index.php';
		$scope.to_home();
	};
	
	//Sets now playing when user opens page
	
	// $interval(function(){
		// $http({
			// url: 'do_to_db.php',
			// method: 'GET',
			// params: {
				// type: 'set_status'
			// }
			// }).success(function(){
		// });
	// },10000);
	
	
	
	
	$scope.update_videos = function() {
		$http({
			url: 'do_to_db.php',
			method: 'get',
			params: {
				type: 'update_videos'
			}
			}).success(function(val) {
			$rootScope.sch = val.schedule;
			$scope.update_videos();
		});
	};
	
	$scope.get_schedule = function() {
		$http({
			url: 'get.php',
			method: 'get',
			params: {
				type: 'get_schedule'
			}
			}).success(function(val) {
			$scope.sch = val.schedule;
			$scope.schedule = val.schedule;
			$scope.update_videos();
		});
	};
	
	
	$scope.switch_front = function() {
		$("transition").css({
			'transform': 'translateZ(0px) translateY(100px) translateX(0px) rotateY(-30deg)',
			'opacity':'0'
		});
	};
	
	$scope.switch_back = function() {
		$("transition").css({
			'transform': 'translateZ(0px) translateY(0px) translateX(0px) rotateY(0deg)',
			'opacity':'1'
		});
		$(".video_info").css({
			'opacity':'1'
		});
	};
	
	$scope.dismiss_notif = function(delay) {
		$timeout(
		function() {
			$rootScope.error_notif_show = "";
			$rootScope.notif_message = "";
		},delay
		);
	};
	
	$scope.set_notif = function(msg) {
		$rootScope.error_notif_show = "error_notif_show";
		$rootScope.notif_message = msg;
	};
	
	$scope.td_height = function(hours,index){
		$hours = '';
		if(hours > 2) {
			$scope.td_heighten[index] = "heighten_td";
			} else {
			$scope.td_heighten[index] = "";
		}
	};
})
.controller('requests',function($scope,$rootScope,$http,localStorageService){
	$scope.request_show = function(show){
		var request_name_regex = /^[a-zA-Z0-9]+([\s][a-zA-Z0-9]+)*$/;
		if(request_name_regex.test(show) && show != undefined && show != ''){
		$http({
			url: 'shows.php',
			method: 'get',
			params: {
				from: localStorageService.cookie.get('username'),
				show: show,
				type: 'request_show'
			}
			}).success(function(val){
			if (val) {
				alert("Requested!");
				$(".request").blur();
				$scope.load_requests();
				$scope.show = '';
			}
		});
		} 
		else {
			$scope.set_notif('Invalid Input');
			$scope.dismiss_notif(3000);
		}
	};
	
	$scope.load_requests = function(){
		$http({
			url: 'shows.php',
			method: 'get',
			params: {
				from: localStorageService.cookie.get('username'),
				admin: "false",
				type: 'load_requests'
			}
			}).success(function(val){
			if (val) {
				$scope.user_requests = val;
				} else {
				alert("Couldn't load requests");
			}
		});
	};
})
.controller('profile',function($scope,$rootScope,$http,localStorageService,$location,$window){
	$scope.blur_all = function(){
		$(".blur").css({
			'opacity':'0.2'
		});
	};
	
	$scope.choose_avatar = function(avatar){
		$http({
			url: 'do_to_db.php',
			method: 'get',
			params: {
				avatar: avatar,
				username: localStorageService.cookie.get('username'),
				type: 'update_avatar'
			}
			}).success(function(val){
				$rootScope.avatar_url = 'assets/img/avatars/' + avatar + '.png';
				localStorageService.cookie.set('avatar_url',$rootScope.avatar_url);
		});
	};
	
	$scope.finish_profile = function(){
		// localStorageService.cookie.set('avatar',);
		$(".blur").css({
			'opacity':'1'
		});
		$window.location.href = 'index.php';
	};
})
.config(function($routeProvider, $locationProvider, localStorageServiceProvider) {
	$routeProvider
	.when('/home', {
		templateUrl: 'app/home.html',
		controller: 'ctvapp_controller'
	})
	.when('/request', {
		templateUrl: 'app/request.html',
		controller: 'ctvapp_controller'
	})
	.when('/create_profile', {
		templateUrl: 'app/create_profile.html',
		controller: 'profile'
	})
	.when('/avatar', {
		templateUrl: 'app/avatar.html',
		controller: 'profile'
	})
	.when('/help', {
		templateUrl: 'app/help.html',
		controller: 'ctvapp_controller'
	})
	.when('/schedule', {
		templateUrl: 'app/schedule.html',
		controller: 'ctvapp_controller'
	});
	
	localStorageServiceProvider
	.setPrefix('ctv')
	.setStorageType('localStorage')
	.setStorageCookie(365, '/');
	
	// configure html5 to get links working on jsfiddle
	// $locationProvider.html5Mode(true);
});	