var app = angular.module('ctvapp',['ngRoute','LocalStorageModule'])
.run(function($http,$rootScope,localStorageService,$location,$window){
	//Initializations
	$rootScope.td_heighten = [];
	$rootScope.login_tab = true;
	$rootScope.drop_down = false;
	
	if(localStorageService.cookie.get('username') == null) {
		
		$rootScope.login_text = "Login";
		$rootScope.isLoggedIn = false;
		localStorageService.cookie.set('username',null);
		$window.location.href = 'index.php';
		
		} else {
		
		$http({	
			url: 'users.php',
			method: 'get',
			params: {
				username: localStorageService.cookie.get('username'),
				seraph_id: localStorageService.cookie.get('seraph_id'),
				type: 'login_or_create'
			}
			}).success(function(val){
			if (val.status == "exists") {
				
				$rootScope.user_details = val;
				$rootScope.isLoggedIn = true;
				$rootScope.login_text = localStorageService.cookie.get('username');
				$rootScope.avatar_url = 'assets/img/avatars/' + val.avatar + '.png';
				localStorageService.cookie.set('avatar_url',$rootScope.avatar_url);
				if(val.admin){
					$location.path('home');
				} 
				else 
				{
					$window.location.href = 'index.php';
				}
			} 
			// else if(val.status == 'created') {
			
			// $rootScope.isLoggedIn = true;
			// $rootScope.login_text = localStorageService.cookie.get('username');
			// $location.path('create_profile');
			
			// } else if (val.status == 'insert_error') {
			
			// $scope.set_notif("Error creating profile!");
			// $scope.dismiss_notif(3000);
			// $location.path('home');
			// }
		});
	}
})
.controller('ctvapp_controller',function($route,$scope,$location,$window,$rootScope,$timeout,$http,$interval,localStorageService){
	
	$rootScope.$on('$routeChangeStart',function(event, next, current){
		if(next.templateUrl === 'app/home.html'){
			if(current.templateUrl === 'app/make_admin.html' || current.templateUrl === 'app/edit_schedule.html' || current.templateUrl === 'app/request_admin.html' || current.templateUrl === 'app/help.html' || current.templateUrl === 'app/avatar.html') 
			{
				$window.location.href = 'index.php';
			}
		}
	});
	//Initializations
	$scope.login_tab = true;
	$scope.drop_down = false;
	
	
	//Interval functions
	//PIWIK daily count(action)
	$scope.visits = 'unavailable';
	$http({
		url: 'http://analytics.karunya.edu/?module=API&method=Live.getCounters&format=json&token_auth=07a6ebb37779915c4b314d0c5b689649&urls=ctv.karunya.edu&idSite=3&period=day&date=last1&lastMinutes=1440',
		method: 'get'
		}).success(function(val){
		$scope.visits = val.visits;
	});
	
	$interval(function(){
		$http({
			url: 'http://analytics.karunya.edu/?module=API&method=Live.getCounters&format=json&token_auth=07a6ebb37779915c4b314d0c5b689649&urls=ctv.karunya.edu&idSite=3&period=day&date=last1&lastMinutes=1440',
			method: 'get'
			}).success(function(val){
			$scope.visits = val.visits;
		});
	},60000);
	//A function that runs every 1 second when the page is open to check for now playing video
	$scope.get_now_playing = function(){
		$http({
			url: 'get.php',
			method: 'GET',
			params: {
				type: 'get_now_playing'
			}
		}).success(function(val)
		{
			if (val != "no_video_playing") {
				$rootScope.now_playing = val;
				} else {
				$rootScope.now_playing = null;
			}
		});
	};
	
	//Auto route
	$scope.to_home = function() {
		$location.path('home');
	};
	
	//Sets now playing when user opens page
	
	// $interval(function(){
	// $http({
	// url: 'do_to_db.php',
	// method: 'GET',
	// params: {
	// type: 'set_status'
// }
// }).success(function(){
// });
// },1000);

$scope.login_function = function(){
if(localStorageService.cookie.get('username') == null) {
$window.location.href = 'login.php';
} else {
// $scope.login_tab = false;
$scope.drop_down = true;
}
};

$scope.logout = function() {
localStorageService.cookie.set('username',null);
localStorageService.cookie.set('password',null);
$window.location.href = 'index.php';
};

$scope.update_videos = function() {
$http({
url: 'do_to_db.php',
method: 'get',
params: {
type: 'update_videos'
}
}).success(function(val) {
$rootScope.sch = val.schedule;
$scope.update_videos();
});
};

$scope.get_schedule = function() {
$http({
url: 'get.php',
method: 'get',
params: {
type: 'get_schedule'
}
}).success(function(val) {
$scope.sch = val.schedule;
$scope.schedule = val.schedule;
$scope.update_videos();
});
};



$scope.switch_front = function() {
$("transition").css({
'transform': 'translateZ(0px) translateY(0px) translateX(0px) rotateY(0deg)',
'opacity':'1'
});
$(".video_info").css({
'opacity':'1'
});
};

$scope.switch_back = function() {
$("transition").css({
'transform': 'translateZ(0px) translateY(0px) translateX(0px) rotateY(0deg)',
'opacity':'1'
});
$(".video_info").css({
'opacity':'1'
});
};
})
.controller('edit_entry',function($scope,$rootScope,$window,$http,$location){

$scope.insert_entry = function(from,to,program,host){
$http({
url: 'do_to_db.php',
method: 'GET',
params:{
from:from,
program:program,
host:host,
to:to,
type: 'ins_sch'
}
}).success(function(val){
if(val == true){
$scope.get_schedule();
$location.path('schedule');
$rootScope.ins_from = null;
$rootScope.ins_to = null;
$rootScope.ins_host= null;
$rootScope.ins_program = null;
}
else{
alert("Error");
}
});
};

$scope.delete_schedule_entry = function(id){
$http({
url: 'do_to_db.php',
method: 'GET',
params:{
id:id,
type: 'delete_schedule_entry'
}
}).success(function(val){
if(val == true){
//Getting fresh schedule
$scope.get_schedule();
$location.path('schedule');
}
});
};

$scope.edit_entry = function(id){
// var path = '/edit_entry/'.id;
$rootScope.id_to_be_edited = id;
$location.path('edit_entry');
};

$scope.update_entry = function(){
$http({
url: 'do_to_db.php',
method: 'GET',
params:{
from: $scope.from_edit,
program: $scope.program_edit,
host: $scope.host_edit,
id: $rootScope.id_to_be_edited,
to: $scope.to_edit,
type: 'update_data'
}
}).success(function(val){
if(val == 1){
$scope.get_schedule();
$location.path('edit_schedule');
}
});	
};

$scope.make_admin = function(to_be_admin){
$http({
url: 'users.php',
method: 'GET',
params:{
seraph_id: to_be_admin,
type: 'make_admin'
}
}).success(function(val){
alert(JSON.stringify(val));
if(val.status == 'converted_to_admin'){
$window.location.href = 'index.php';
}
else {
$scope.set_notif(val.status);
$scope.dismiss_notif(3000);
}
});
};
$scope.make_normal= function(to_be_normal){
$http({
url: 'users.php',
method: 'GET',
params:{
seraph_id: to_be_normal,
type: 'make_normal'
}
}).success(function(val){
alert(JSON.stringify(val));
if(val.status == 'converted_to_normal'){
$window.location.href = 'index.php';
}
else {
$scope.set_notif(val.status);
$scope.dismiss_notif(3000);
}
});
};
})
.controller('requests',function($scope,$rootScope,$http,localStorageService){

$scope.load_requests = function(){
$http({
url: 'shows.php',
method: 'get',
params: {
from: localStorageService.cookie.get('username'),
admin: 'true',
type: 'load_requests'
}
}).success(function(val){
if (val) {
$rootScope.user_requests = val;
} else {
alert("Couldn't load requests");
}
});
};

$scope.approve_requests = function(from,show,approve){
$http({
url: 'shows.php',
method: 'get',
params: {
from: from,
show: show,
approve: approve,
type: 'approve_requests'
}
}).success(function(val){
if (val.status) {
$scope.load_requests();
} else {
alert("Couldn't approve!");
}
});
};
$scope.flag_requests = function(from,show,flag){
$http({
url: 'shows.php',
method: 'get',
params: {
from: from,
show: show,
flag: flag,
type: 'flag_requests'
}
}).success(function(val){
if (val.status) {
$scope.load_requests();
} else {
alert("Couldn't complete your request!");
}
});
};
$scope.delete_requests = function(from,show){
$http({
url: 'shows.php',
method: 'get',
params: {
from: from,
show: show,
type: 'delete_requests'
}
}).success(function(val){
if (val.status) {
$scope.load_requests();
} else {
alert("Couldn't complete your request!");
}
});
};
})
.controller('profile',function($scope,$rootScope,$http,localStorageService,$location,$window){
$scope.blur_all = function(){
$(".blur").css({
'opacity':'0.2'
});
};

$scope.choose_avatar = function(avatar){
$http({
url: 'do_to_db.php',
method: 'get',
params: {
avatar: avatar,
username: localStorageService.cookie.get('username'),
type: 'update_avatar'
}
}).success(function(val){
$rootScope.avatar_url = 'assets/img/avatars/' + avatar + '.png';
localStorageService.cookie.set('avatar_url',$rootScope.avatar_url);
});
};

$scope.finish_profile = function(){
// localStorageService.cookie.set('avatar',);
$(".blur").css({
'opacity':'1'
});
$location.path('home');
};
})
.config(function($routeProvider, $locationProvider, localStorageServiceProvider) {
$routeProvider
.when('/home', {
templateUrl: 'app/home.html',
controller: 'ctvapp_controller'
})
.when('/avatar', {
templateUrl: 'app/avatar.html',
controller: 'profile'
})
.when('/edit_entry', {
templateUrl: 'app/edit_entry.html',
controller: 'edit_entry'
})
.when('/request', {
templateUrl: 'app/request_admin.html',
controller: 'edit_entry'	
})
.when('/create_profile', {
templateUrl: 'app/create_profile.html',
controller: 'profile'
})
.when('/make_admin', {
templateUrl: 'app/make_admin.html',
controller: 'ctvapp_controller'
})
.when('/help', {
templateUrl: 'app/help.html',
controller: 'ctvapp_controller'	
})
.when('/schedule', {
templateUrl: 'app/edit_schedule.html',
controller: 'edit_entry'
});

localStorageServiceProvider
.setPrefix('ctv')
.setStorageType('localStorage')
.setStorageCookie(365, '/');

// configure html5 to get links working on jsfiddle
// $locationProvider.html5Mode(true);
});		
