-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 18, 2015 at 02:36 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ctv`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages_for_admin`
--

CREATE TABLE IF NOT EXISTS `messages_for_admin` (
`id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `message` longtext NOT NULL,
  `from_user` text NOT NULL,
  `reply` longtext NOT NULL,
  `timestamp_reply` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `messages_for_admin`
--

INSERT INTO `messages_for_admin` (`id`, `timestamp`, `message`, `from_user`, `reply`, `timestamp_reply`) VALUES
(7, '2015-07-07 17:34:56', 'asdfasdfa', 'hello', '', '0000-00-00 00:00:00'),
(8, '2015-07-07 17:37:19', 'Hello partner!', 'wikidjon', 'Hello', '2015-07-15 16:25:05'),
(9, '2015-07-07 17:39:11', 'Test message from user wikidjon', 'wikidjon', 'adsgasd', '2015-07-15 16:49:21'),
(10, '2015-07-07 17:42:59', 'Wow man how are you?', 'wikidjon', '', '0000-00-00 00:00:00'),
(11, '2015-07-07 18:16:17', 'Hello there', 'wikidjon', '', '0000-00-00 00:00:00'),
(12, '2015-07-07 18:31:11', 'Hello There!', 'wikidjon', '', '0000-00-00 00:00:00'),
(13, '2015-07-07 18:34:20', 'Hello', 'wikidjon', '', '0000-00-00 00:00:00'),
(14, '2015-07-07 18:38:20', 'awdalkfjasldkf', 'wikidjon', '', '0000-00-00 00:00:00'),
(15, '2015-07-07 18:38:20', 'awdalkfjasldkf', 'wikidjon', '', '0000-00-00 00:00:00'),
(16, '2015-07-07 18:38:23', 'awdalkfjasldkf', 'wikidjon', '', '0000-00-00 00:00:00'),
(17, '2015-07-07 18:38:24', 'awdalkfjasldkf', 'wikidjon', '', '0000-00-00 00:00:00'),
(18, '2015-07-07 18:38:25', 'awdalkfjasldkf', 'wikidjon', '', '0000-00-00 00:00:00'),
(19, '2015-07-07 18:38:26', 'awdalkfjasldkf', 'wikidjon', '', '0000-00-00 00:00:00'),
(20, '2015-07-07 18:38:26', 'awdalkfjasldkf', 'wikidjon', '', '0000-00-00 00:00:00'),
(21, '2015-07-07 18:38:27', 'awdalkfjasldkf', 'wikidjon', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE IF NOT EXISTS `schedule` (
`id` int(11) NOT NULL,
  `from_time` datetime NOT NULL,
  `to_time` datetime NOT NULL,
  `program` text NOT NULL,
  `host` text NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=68 ;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`id`, `from_time`, `to_time`, `program`, `host`, `status`) VALUES
(67, '2015-08-04 19:00:00', '2015-08-04 21:00:00', 'Joobma', 'Boomba', '');

-- --------------------------------------------------------

--
-- Table structure for table `show_requests`
--

CREATE TABLE IF NOT EXISTS `show_requests` (
`id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `show_name` longtext NOT NULL,
  `from_user` text NOT NULL,
  `approve` text NOT NULL,
  `flag` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `show_requests`
--

INSERT INTO `show_requests` (`id`, `timestamp`, `show_name`, `from_user`, `approve`, `flag`) VALUES
(1, '2015-08-18 18:05:30', 'awdaw', ' Jonathan Vijayakumar', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `seraph_id` text NOT NULL,
  `admin` int(11) NOT NULL,
  `avatar` int(11) NOT NULL,
  `theme` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `seraph_id`, `admin`, `avatar`, `theme`) VALUES
(1, ' Jonathan Vijayakumar', '', 'UR12CS100', 1, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
  `id` int(11) NOT NULL,
  `from_time` datetime NOT NULL,
  `to_time` datetime NOT NULL,
  `program` text NOT NULL,
  `host` text NOT NULL,
  `likes` longtext NOT NULL,
  `dislikes` longtext NOT NULL,
  `repeat` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `messages_for_admin`
--
ALTER TABLE `messages_for_admin`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `show_requests`
--
ALTER TABLE `show_requests`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
 ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `messages_for_admin`
--
ALTER TABLE `messages_for_admin`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `show_requests`
--
ALTER TABLE `show_requests`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
