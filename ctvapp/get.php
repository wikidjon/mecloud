<?php
	
	//Specifying that output is in JSON format
	header("Content-Type: application/json; charset=UTF-8");
	include 'db_connect.php';
	ini_set('date.timezone', 'Asia/Kolkata');
	$now = date('Y-m-d H:i:s');
	
	if ($_GET['type'] == 'get_schedule') {
		$query = "select * from schedule";
		$result = $conn->query($query) or die(mysql_error());
		$output = "";
		$hours = '';
		$from = '';
		$to = '';
		
		//Fetch data and make a make a JSON object out of it
		
		while($data = $result -> fetch(PDO::FETCH_ASSOC))
		{
			$count = 1;	
			if($output != ""){
				$output .= ",";
			}
			
			foreach($data as $vals)
			{
				if($count == 1)
				$output .= '{"id" : "'.$vals.'",';
				else if($count == 2) {
					$output .= '"from" : "'.$vals.'",';
					$from = $vals;
				}
				else if($count == 3) {
					$output .= '"to" : "'.$vals.'",';
					$to = $vals;
					$hours = strtotime($to) - strtotime($from);
					$hours /= 3600;
					$output .= '"hours" : "'.$hours.'",';
				}
				else if($count == 4)
				$output .= '"program" : "'.$vals.'",';
				else if($count == 5)
				$output .= '"host" : "'.$vals.'",';
				else if($count == 6)
				$output .= '"status" : "'.$vals.'"}';
				else if($count == 7) {
					$count = 0;
				}
				$count += 1;
			}
			
		}
		//Print the JSON object
		echo '{"schedule" : ['.$output.']}';
	} 
	
	else if ($_GET['type'] == 'get_now_playing') 
	{
		
		$query = 'select * from schedule where from_time <= "'.$now.'" AND to_time > "'.$now.'"';
		
		if($result = $conn->query($query) or die(mysql_error()) != FALSE){
			
			//Fetch data and make a make a JSON object out of it
			
			if($data = $result->fetch(PDO::FETCH_ASSOC)){
				
				//This block executes if there is a video currently playing ...ie 'now' is between the video's from and to
				$id = $data['id'];
				$set_status_now_playing = 'update schedule set status = "Now playing" where id = '.$id;
				$set_status_reset= 'update schedule set status = ""';
				$result = $conn->query($set_status_reset) or die(mysql_error());
				$result = $conn->query($set_status_now_playing) or die(mysql_error());
				echo json_encode($data);
			}
			else{
				//This block executes if no video is currently playing
				$set_status_reset = 'update schedule set status = ""';
				$result = $conn->query($set_status_reset) or die(mysql_error());
				echo "no_video_playing";
			}
		}
	}
?>